//Import required modules
import {app} from './app.js';
import {print_to_console} from './print_to_console.js';
import {run_long_request} from './run_long_request.js';
// import {btn_feedback} from './btn_feedback.js';
import {findMatches} from './users_list.js';

//Global availibility
window.app = app;
window.log = print_to_console;
window.longReq = run_long_request;
window.fc = findMatches;

// alternative to DOMContentLoaded
document.onreadystatechange = () => {
    if (document.readyState == "interactive") {
        // btn_feedback();
    }
}