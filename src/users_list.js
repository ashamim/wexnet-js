let findMatches=(wordToMatch)=>{
    const endpoint = "https://raw.githubusercontent.com/lutangar/cities.json/master/cities.json";
    let cities = [];
    fetch(endpoint)
        .then(a => a.json())
        .then(b => cities.push(...b))

    return cities.filter(place => {
        const regex = new regex(wordToMatch, 'gi')
        return place.city.match(regex) || place.state.match(regex)
    })
}

export {findMatches};