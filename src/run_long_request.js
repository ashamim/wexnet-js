/*
    On Page submit show the working preloader and run the process in the background
*/
let run_long_request = (request, warnmsg) => {
    if (!warnmsg || confirm(warnmsg)) {
        apex.submit({
            request:request,
            showWait:true
        })
    }
}
export {run_long_request};