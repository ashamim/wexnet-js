# README #

Wexnet Javascript

### What is this repository for? ###

* Rewrite Wexnet JS code in ES6 and build using babel, webpack, sourcetree, bitbucket for SVN and jenkins for deployment.
* 1.0.0

### How do I get set up? ###

* Summary of set up
	1. [Download Bitbucket Repository](https://ashamim@bitbucket.org/ashamim/wexnet-js.git)
* Configuration
	1. npm init -y * one time and already done for the project
		package.json
	2. npm install --save-dev webpack -g *if first time
		webpack.config.js
	3. npm install babel-core babel-loader webpack-dev-server babel-preset-es2015 babel-polyfill --save-dev *if first time
	4. npm install loglevel *if first time

	5. npm run dev * build the file and serve on url localhost:786 for testing
	6. npm run build * build the file
	7. npm run build-min * minified build the file

	8. src folder
		index.js
	9. build folder
		test.html
		wexnet-bundle.js will be the outputfile

* Dependencies
	1. Nodejs 8.2.1
	2. Webpack 3.3.0
	3. Babel 
* Database configuration
* How to run tests
* Deployment instructions

### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###

* Repo owner or admin
* Other community or team contact