/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, {
/******/ 				configurable: false,
/******/ 				enumerable: true,
/******/ 				get: getter
/******/ 			});
/******/ 		}
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = 0);
/******/ })
/************************************************************************/
/******/ ([
/* 0 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


var _app = __webpack_require__(1);

var _print_to_console = __webpack_require__(2);

var _run_long_request = __webpack_require__(3);

var _users_list = __webpack_require__(4);

//Global availibility
//Import required modules
window.app = _app.app;
// import {btn_feedback} from './btn_feedback.js';

window.log = _print_to_console.print_to_console;
window.longReq = _run_long_request.run_long_request;
window.fc = _users_list.findMatches;

// alternative to DOMContentLoaded
document.onreadystatechange = function () {
    if (document.readyState == "interactive") {
        // btn_feedback();
    }
};

/***/ }),
/* 1 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
    value: true
});
var app = {
    id: document.querySelector('#pFlowId').value,
    name: 'WEXNET 2.0.0',
    page: {
        id: document.querySelector('#pFlowStepId').value,
        name: '',
        title: document.title
    }
};

exports.app = app;

/***/ }),
/* 2 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
	value: true
});
var print_to_console = function print_to_console() {
	var _console;

	(_console = console).log.apply(_console, arguments);
};
exports.print_to_console = print_to_console;

/***/ }),
/* 3 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
    value: true
});
/*
    On Page submit show the working preloader and run the process in the background
*/
var run_long_request = function run_long_request(request, warnmsg) {
    if (!warnmsg || confirm(warnmsg)) {
        apex.submit({
            request: request,
            showWait: true
        });
    }
};
exports.run_long_request = run_long_request;

/***/ }),
/* 4 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
    value: true
});

function _toConsumableArray(arr) { if (Array.isArray(arr)) { for (var i = 0, arr2 = Array(arr.length); i < arr.length; i++) { arr2[i] = arr[i]; } return arr2; } else { return Array.from(arr); } }

var findMatches = function findMatches(wordToMatch) {
    var endpoint = "https://raw.githubusercontent.com/lutangar/cities.json/master/cities.json";
    var cities = [];
    fetch(endpoint).then(function (a) {
        return a.json();
    }).then(function (b) {
        return cities.push.apply(cities, _toConsumableArray(b));
    });

    return cities.filter(function (place) {
        var regex = new regex(wordToMatch, 'gi');
        return place.city.match(regex) || place.state.match(regex);
    });
};

exports.findMatches = findMatches;

/***/ })
/******/ ]);